(defn self-divisor? [in1] 
  (
    (fn [inp inp2]
      (cond 
        (= inp2 0) true
        (= (mod inp (mod inp2 10)) 0) (recur inp (int (/ inp2 10)))
        :else false
      )
    )
    in1 in1)
)
(prn (self-divisor? (Integer. (read-line))))
