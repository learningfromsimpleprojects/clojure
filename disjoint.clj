(defn disjoint? [a b]
  (let [A (vec (sort a)) B (vec (sort b))]
    (cond
      (or (empty? A) (empty? B)) true
      (= (get A 0) (get B 0)) false
      (> (get A 0) (get B 0)) (recur A (subvec B 1))
      (< (get A 0) (get B 0)) (recur (subvec A 1) B)))
  )