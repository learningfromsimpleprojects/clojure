(def secret (rand-int 10000))

(prn "Guess the number!")

(defn game [counter]
  (prn "Input your guess: ")
  (let [inp (Integer. (read-line))]
    (cond
      (> inp secret)
        (do
          (prn "Too high try again.")
          (game (inc counter))
        )
      (< inp secret)
        (do
          (prn "Too low try again.")
          (game (inc counter))
        )
      :else 
        (prn (str "You won! In this many tries: " counter))
    )
  )
)

(game 0)