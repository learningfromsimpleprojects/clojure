(defn hang [body_num]
  (defn modify_body [body_vec num]
    (cond
      (= num 0) body_vec
      (= num 1) (assoc body_vec 0 " 0 ")
      (= num 2) (assoc (assoc (modify_body body_vec 1) 1 " | ") 2 " | ")
      (= num 3) (assoc (modify_body body_vec 2) 1 "/| ")
      (= num 4) (assoc (modify_body body_vec 2) 1 "/|\\")
      (= num 5) (assoc (modify_body body_vec 4) 3 "/  ")
      (= num 6) (assoc (modify_body body_vec 4) 3 "/ \\")))
  (let [body_mat ["   ", "   ", "   ", "   "]]
    (println "____ ")
    (loop [modded_body (modify_body body_mat body_num) num 0]
      (if (< num 4)
        (do
          (println (str "| " (get modded_body num)))
          (recur modded_body (inc num)))))
    (println "|____")))
(let [secret ["m","a","t"]];change to use string mani. not str[]
  (loop [num 0 known ["_","_","_"]]
    (hang num)
    (println (reduce str known))
    (println "give me your guess: ")
    (cond
      (not (some #(= "_" %) known)) (println "you win")
      (> num 5) (println "you lose")
      :else (do
              (let [inpu (read-line)]
                (if (some #(= inpu %) secret)
                  (recur num (map (fn [k s] (if (= k inpu) inpu s)) secret known))
                  (recur (inc num) known)))))))